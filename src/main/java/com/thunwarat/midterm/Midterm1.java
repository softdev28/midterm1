/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.midterm;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Midterm1 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        String S = kb.next();
        System.out.println(StrToInt(S));
    }

    public static int StrToInt(String S) {
        int res = 0;
        for (int i = 0; i <= S.length() - 1; i++) {
            res = res * 10 + S.charAt(i) - '0';
        }
        return res;
    }
}
